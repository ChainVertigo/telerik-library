import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';

import cors from 'cors';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cors());
  app.use(helmet());
  await app.listen(3000);
}
bootstrap();
