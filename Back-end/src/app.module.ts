import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from './core/core.module';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { BooksModule } from './books/books.module';
import { ReviewsModule } from './reviews/reviews.module';

import { AppController } from './app.controller';
import { AuthController } from './auth/auth.controller';
import { BooksController } from './books/books.controller';
import { ReviewsController } from './reviews/reviews.controller';

import { ConfigService } from './config/config.service';
// import { UserModule } from './user/user.module';

// type, host, port, username, password, database, entities

@Module({
  imports: [
    ReviewsModule,
    BooksModule,
    CoreModule,
    AuthModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
      }),
    }),
    // UserModule,
  ],
controllers: [AppController, AuthController, BooksController, ReviewsController],
  providers: [],
})
export class AppModule {}
