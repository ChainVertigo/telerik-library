export enum ratingScale {
  Unreadable = 1,
  Meh = 2,
  Ok = 3,
  Good = 4,
  Amazing = 5,
}
