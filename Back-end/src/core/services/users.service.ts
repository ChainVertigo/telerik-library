import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';

import { Repository } from 'typeorm';

import { User } from '../../data/entities/user';

import { UserLoginDTO } from '../../models/user/user-login-dto';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { ShowUserDTO } from '../../models/user/user-show-dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async register(user: UserRegisterDTO): Promise<ShowUserDTO> {
    const newUser: User = this.usersRepository.create(user);

    const passwordHash = await bcrypt.hash(user.password, 10);
    newUser.password = passwordHash;

    const savedUser = await this.usersRepository.save(newUser);

    return this.convertToShowUserDTO(savedUser);
  }

  async findUserById(id: string): Promise<ShowUserDTO> {
    const foundUser = await this.usersRepository.findOne({
      where: {
        id,
        isDeleted: false,
      },
    });

    if (!foundUser) {
      throw new BadRequestException('User with this ID does not exist.');
    }

    const convertedUser = await this.convertToShowUserDTO(foundUser);

    return convertedUser;
  }

  async findUserByEmail(email: string): Promise<ShowUserDTO> | undefined {
    const foundUser = await this.usersRepository.findOne({
      where: {
        email,
      },
    });

    if (!foundUser) {
      return undefined;
    }

    return this.convertToShowUserDTO(foundUser);
  }

  async findUserByUsername(name: string): Promise<ShowUserDTO> | undefined {
    const foundUser = await this.usersRepository.findOne({
      where: {
        name,
      },
    });

    if (!foundUser) {
      return undefined;
    }

    return this.convertToShowUserDTO(foundUser);
  }

  async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
    const userEntity = await this.usersRepository.findOne({
      where: {
        name: user.name,
      },
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }

  private async convertToShowUserDTO(user: User): Promise<ShowUserDTO> {

    const convertedUser: ShowUserDTO = {
      id: user.id,
      name: user.name,
      email: user.email,
      createdOn: user.joined,
    };

    return convertedUser;
  }
}
