import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { AuthService } from '../auth.service';
import { ConfigService } from '../../config/config.service';

import { JwtPayload } from '../../core/interfaces/jwt-payload';
import { ShowUserDTO } from '../../models/user/user-show-dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
    });
  }

  async validate(payload: JwtPayload): Promise<ShowUserDTO> {
    const user = await this.authService.validateIfUserExists(payload.name);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
