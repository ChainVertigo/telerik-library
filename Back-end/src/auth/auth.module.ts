import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../config/config.module';
import { CoreModule } from '../core/core.module';

import { AuthController } from './auth.controller';

import { AuthService } from './auth.service';

import { JwtStrategy } from './strategy/jwt.strategy';
import { config } from '../common/config';

const jwtPassportModule =  PassportModule.register({defaultStrategy: 'jwt'})

@Module({
  imports: [
    CoreModule,
    ConfigModule,
    jwtPassportModule,
    JwtModule.registerAsync({
      // Use the actual ConfigService, not the hardcoded config object
      useFactory: async () => ({
        secretOrPrivateKey: config.jwtSecret,
        signOptions: {
          expiresIn: config.expiresIn, // one hour
        },
      }),
    }),
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService, jwtPassportModule],
})
export class AuthModule {}
