import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';

import { UserLoginDTO } from '../models/user/user-login-dto';
import { ShowUserDTO } from '../models/user/user-show-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) { }

  async login(user: UserLoginDTO): Promise<{ user: ShowUserDTO, token: string }> {
    if (!user.password && !user.name) {
      throw new BadRequestException('Ooops something went wrong!');
    }
    const isUserPasswordValid = await this.validateUserPassword(user);

    if (!isUserPasswordValid) {
      throw new BadRequestException('Ooops something went wrong! - valid');
    }
    const userFound = await this.usersService.findUserByUsername(user.name);

    const token = await this.jwtService.sign({ name: userFound.name });

    return { user: userFound, token };
  }

  async register(user: UserRegisterDTO): Promise<ShowUserDTO> {
    return await this.usersService.register(user);
  }

  async validateIfUserExists(name: string): Promise<ShowUserDTO> | undefined {
    return await this.usersService.findUserByUsername(name);
  }

  async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
    return await this.usersService.validateUserPassword(user);
  }
}
