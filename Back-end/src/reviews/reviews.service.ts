
import { Injectable, BadRequestException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ShowReviewDTO } from '../models/review/review-show-dto';
import { CreateUpdateReviewDTO } from '../models/review/create-update-review-dto';

import { User } from '../data/entities/user';
import { Book } from '../data/entities/book';
import { Review } from '../data/entities/review';

@Injectable()
export class ReviewsService {

  constructor(
    @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Book) private readonly bookRepostitory: Repository<Book>,
  ) { }

  async create(bookId: string, review: CreateUpdateReviewDTO, user: User): Promise<ShowReviewDTO> {
    const newReview: Review = new Review();
    newReview.author = Promise.resolve(user);

    const book: Book = await this.bookRepostitory.findOne({
      where: {
        id: bookId,
      },
    });

    newReview.book = Promise.resolve(book);
    newReview.content = review.content;
    const savedReview = await this.reviewRepository.save(newReview);
    return this.convertToShowReviewDTO(savedReview);
  }

  async findAllReviews(): Promise<ShowReviewDTO[]> {
    const foundReviews = await this.reviewRepository.find({
      where: {
        isDeleted: false,
      },
    });

    if (!foundReviews) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const convertedReviews: ShowReviewDTO[] = [];

    foundReviews.map(async review => {
      const convertedReview: ShowReviewDTO = { ... await this.convertToShowReviewDTO(review) };
      convertedReviews.push(convertedReview);
    });
    return convertedReviews;
  }

  async findOneById(id: string): Promise<ShowReviewDTO> {
    const foundReview = await this.reviewRepository.findOne({
      where: {
        id,
        isDeleted: false,
      },
    });

    if (!foundReview) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const convertedReview = await this.convertToShowReviewDTO(foundReview);

    return convertedReview;
  }

  public async findAllByUserId(userId: string): Promise<ShowReviewDTO[]> {
    const foundReviews = await this.reviewRepository.find({ relations: ['author'], where: { author: { id: userId }, isDeleted: false } });
    const convertedReviews: ShowReviewDTO[] = [];

    foundReviews.map(async review => {
      const convertedReview: ShowReviewDTO = { ... await this.convertToShowReviewDTO(review) };
      convertedReviews.push(convertedReview);
    });

    return convertedReviews;
  }

  public async findAllByBookId(bookId: string): Promise<ShowReviewDTO[]> {
    const existingBook = await this.bookRepostitory.findOne({ where: { id: bookId } });

    if (!existingBook) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const foundReviews = await this.reviewRepository.find({ relations: ['author'], where: { book: { id: bookId }, isDeleted: false } });
    const convertedReviews: ShowReviewDTO[] = [];

    foundReviews.map(async review => {
      const convertedReview: ShowReviewDTO = { ... await this.convertToShowReviewDTO(review) };
      convertedReviews.push(convertedReview);
    });

    return convertedReviews;
  }

  async update(id: string, review: CreateUpdateReviewDTO, user: User, bookId: string): Promise<ShowReviewDTO> {
    const reviewToUpdate = await this.reviewRepository.findOne({
      where: { id },
    });

    if (!reviewToUpdate) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    reviewToUpdate.author = Promise.resolve(user);
    const book: Book = await this.bookRepostitory.findOne({
      where: {
        id: bookId,
      },
    });

    reviewToUpdate.book = Promise.resolve(book);
    reviewToUpdate.content = review.content;
    const updateReview = await this.reviewRepository.save(reviewToUpdate);

    return this.convertToShowReviewDTO(updateReview);
  }

  async deleteReview(id: string): Promise<ShowReviewDTO> {
    const reviewToDelete = await this.reviewRepository.findOne({
      where: { id },
    });

    if (!reviewToDelete) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    reviewToDelete.isDeleted = true;
    const deletedReview = await this.reviewRepository.save(reviewToDelete);

    return this.convertToShowReviewDTO(deletedReview);
  }

  private async convertToShowReviewDTO(review: Review): Promise<ShowReviewDTO> {

    const convertedReview: ShowReviewDTO = {
      id: review.id,
      author: await review.author,
      content: review.content,
      votes: review.votes,
      flags: review.flags,
      isDeleted: review.isDeleted,
      createdOn: review.createdOn,
      updatedOn: review.updatedOn,
    };

    return convertedReview;
  }
}
