import { Review } from '../data/entities/review';
import { Book } from '../data/entities/book';
import { Role } from '../data/entities/role';
import { User } from '../data/entities/user';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';

import { ReviewsService } from './reviews.service';

import { ReviewsController } from './reviews.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Review, Book, User, Role]), AuthModule],
  providers: [ReviewsService],
  controllers: [ReviewsController],
  exports: [ReviewsService],
})
export class ReviewsModule {}
