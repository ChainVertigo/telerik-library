import { Controller, UseGuards, Get, Param, Post, Body, Req, Put, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ReviewsService } from './reviews.service';
import { ShowReviewDTO } from '../models/review/review-show-dto';

import { CreateUpdateReviewDTO } from '../models/review/create-update-review-dto';

@UseGuards(AuthGuard())
@Controller('api')
export class ReviewsController {

  constructor(
    private readonly reviewService: ReviewsService,
  ) { }

  @Post('books/:id/reviews')
  async createReview(
    @Param('id') bookId: string,
    @Req() request,
    @Body('review') review: CreateUpdateReviewDTO,
  ): Promise<ShowReviewDTO> {
    return this.reviewService.create(bookId, review, request.user);
  }

  @Get('users/:id/reviews')
  async getAllReviewsByUserId(@Param('id') id): Promise<ShowReviewDTO[]> {
    return this.reviewService.findAllByUserId(id);
  }

  @Get('books/:id/reviews')
  async getAllReviewsByBookId(@Param('id') id): Promise<ShowReviewDTO[]> {
    return this.reviewService.findAllByBookId(id);
  }

  @Put('books/:id/reviews/:id')
  async updateReview(
    @Param('id') id: string,
    @Body('review') review: CreateUpdateReviewDTO,
    @Req() request,
    @Param('bookId') bookId,
  ): Promise<ShowReviewDTO> {
    return await this.reviewService.update(id, review, request.user, bookId);
  }

  @Delete('books/:id/reviews/:id')
  async deleteReview(
    @Param('id') id: string,
  ): Promise<ShowReviewDTO> {
    return await this.reviewService.deleteReview(id);
  }
}
