export class ShowUserDTO {
  id: string;
  name: string;
  email: string;
  createdOn: Date;
}
