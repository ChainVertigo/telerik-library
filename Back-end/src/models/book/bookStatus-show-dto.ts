import { User } from '../../data/entities/user';

import { Book } from '../../data/entities/book';

export class ShowBookStatusDTO {
  id: string;
  user: User;
  book: Book;
  borrowedOn: Date;
  returnedOn: Date;
}