import { BookStatus } from "../../data/entities/book-status";

export class ShowBookDTO {
  id: string;
  name: string;
  description: string;
  author: string;
  isBorrowed: boolean;
  isUnlisted: boolean;
}
