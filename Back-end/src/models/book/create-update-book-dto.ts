
export class CreateUpdateBookDTO {
    name: string;
    author: string;
    description: string;
    isUnlisted: boolean;
    isBorrowed: boolean;
}
