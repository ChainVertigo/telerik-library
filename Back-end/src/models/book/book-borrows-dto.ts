import { ShowBookDTO } from './book-show-dto';
import { ShowBookStatusDTO } from './bookStatus-show-dto';


export class BookBorrowsDTO {
  book: ShowBookDTO;
  statuses: ShowBookStatusDTO[];
}