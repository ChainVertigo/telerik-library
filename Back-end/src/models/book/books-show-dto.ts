import { ShowBookDTO } from './book-show-dto';

export class ShowBooksDTO {
  books: ShowBookDTO[];
  count: number;
}