import { ShowBookDTO } from "./book-show-dto";

export class BooksDTO {
  books: ShowBookDTO[];
}
