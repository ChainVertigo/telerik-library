import { Flag } from '../../data/entities/flag';
import { Vote } from '../../data/entities/vote';
import { User } from '../../data/entities/user';

export class ShowReviewDTO {
  id: string;
  author: User;
  content: string;
  votes: Vote[];
  flags: Flag[];
  isDeleted: boolean;
  createdOn: Date;
  updatedOn: Date;
}
