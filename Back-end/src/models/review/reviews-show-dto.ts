import { ShowReviewDTO } from './review-show-dto';

export class ShowReviewsDTO {
  reviews: ShowReviewDTO[];
  count: number;
}
