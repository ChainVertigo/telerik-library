import { Controller, Get, UseGuards, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { RolesGuard } from './common/guards/roles.guard';
import { UserRole } from './common/enums/user-role.enum';

import { Roles } from './common/decorators/roles.decorator';
import { User } from './common/decorators/user.decorator';
import { BooksService } from './books/books.service';

@Controller()
export class AppController {
  constructor(
    private readonly bookService: BooksService,
  ) { }
  @Get()
  root(
    @Query('page') page: number = 1,
    @Query('pageSize') pageSize: number = 10,
  ) {

    return this.bookService.creatorsChoice(page, pageSize);
}

@Get('/admin')
@Roles(UserRole.Admin)
@UseGuards(AuthGuard(), RolesGuard)
admin(@User() authenticatedUser) {

  return {
    data: `Yay, you are an admin!`,
  };
}
}
