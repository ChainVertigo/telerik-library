import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, CreateDateColumn } from 'typeorm';

import { Rating } from './rating';
import { Review } from './review';
import { BookStatus } from './book-status';

@Entity('books')
export class Book {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  author: string;

  @Column()
  description: string;

  @Column({
    name: 'isBorrowed',
    default: false,
  })
  isBorrowed: boolean;

  @Column({
    name: 'isUnlisted',
    default: false,
  })
  isUnlisted: boolean;

  @OneToMany(type => BookStatus, bookStatus => bookStatus.book)
  bookStatuses: BookStatus[];

  @ManyToOne(type => Rating, { eager: true })
  ratings: Rating[];

  @OneToMany(type => Review, review => review.id)
  reviews: Promise<Review[]>;

  @CreateDateColumn()
  createdOn: Date;

  @CreateDateColumn()
  updatedOn: Date;
}
