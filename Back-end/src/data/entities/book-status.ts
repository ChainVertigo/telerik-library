import { Entity, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, Column } from 'typeorm';

import { User } from './user';
import { Book } from './book';

@Entity('booksStatus')
export class BookStatus {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => User, user => user.borrowed)
  user: User;

  @ManyToOne(type => Book, book => book.bookStatuses)
  book: Book;

  @CreateDateColumn()
  borrowedOn: Date;

  @Column({nullable: true})
  returnedOn: Date;

}
