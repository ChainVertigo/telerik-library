import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, ManyToOne } from 'typeorm';

import { Review } from './review';
import { User } from './user';

@Entity('flags')
export class Flag {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => User, user => user.flags)
  flagedBy: User;

  @ManyToOne(type => Review, review => review.flags)
  flagedReview: Review;

  @Column('nvarchar')
  reason: string;

  @CreateDateColumn()
  flagedOn: Date;

}
