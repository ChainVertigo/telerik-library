import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  VersionColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { Role } from './role';
import { Rating } from './rating';
import { Review } from './review';
import { Flag } from './flag';
import { BookStatus } from './book-status';
import { Vote } from './vote';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  name: string;

  @Column()
  password: string;

  @Column({ unique: true })
  email: string;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(type => Rating, rating => rating.user)
  ratings: Rating [];

  @CreateDateColumn()
  joined: Date;

  @VersionColumn()
  version: number;

  @OneToMany(type => Review, review => review.id)
  reviews: Promise<Review[]>;

  @Column({
    name: 'isDeleted',
    default: false,
  })
  IsDeleted: boolean;

  @OneToMany(type => Flag, flag => flag.flagedBy)
  flags: Flag[];

  @ManyToOne(type => Vote, vote => vote.votedUser)
  votes: Vote[];

  @OneToMany(type => BookStatus, bookStatus => bookStatus.user)
  borrowed: BookStatus[];

}
