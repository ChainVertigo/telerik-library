import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';

import { ratingScale } from '../../common/enums/rating-scale';
import { Book } from './book';
import { User } from './user';

@Entity('ratings')
export class Rating {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  rating: ratingScale;

  @ManyToOne(type => Book, book => book.ratings)
  book: Book;

  @ManyToOne(type => User, user => user.ratings)
  user: User;
}
