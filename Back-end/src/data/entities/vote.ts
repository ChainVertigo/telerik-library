import { Entity, Column, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne } from 'typeorm';
import { Review } from './review';
import { User } from './user';

@Entity('votes')
export class Vote {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => User, user => user.votes)
  votedUser: User;

  @ManyToOne(type => Review, review => review.votes)
  votedReview: Review;

  @CreateDateColumn()
  votedOn: Date;

}
