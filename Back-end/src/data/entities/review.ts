import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, JoinColumn, OneToMany } from 'typeorm';

import { User } from './user';
import { Book } from './book';
import { Flag } from './flag';
import { ValidationTypes } from 'class-validator';
import { Vote } from './vote';

@Entity('reviews')
export class Review {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  content: string;

  @OneToMany(type => Vote, vote => vote.votedReview)
  votes: Vote[];

  @OneToMany(type => Flag, flag => flag.flagedReview)
  flags: Flag[];

  @Column({
    name: 'isDeleted',
    default: false,
  })
  isDeleted: boolean;

  @ManyToOne(type => User, user => user.id)
  author: Promise<User>;

  @ManyToOne(type => Book, book => book.id)
  book: Promise<Book>;

  @CreateDateColumn()
  createdOn: Date;

  @CreateDateColumn()
  updatedOn: Date;
}
