import { createConnection, Repository } from 'typeorm';

import { UserRole } from '../../common/enums/user-role.enum';

import { User } from '../entities/user';
import { Role } from '../entities/role';
import { Book } from '../entities/book';

import { CreateUpdateBookDTO } from '../../models/book/create-update-book-dto';

// run: `npm run seed` to seed the database

const seedBooks = async connection => {
  const bookRepo: Repository<Book> = connection.manager.getRepository(Book);

  const book1: CreateUpdateBookDTO = {
    name: 'The shining',
    author: 'Stephen King',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isUnlisted: false,
    isBorrowed: false,
  };

  const book2: CreateUpdateBookDTO = {
    name: 'It',
    author: 'Stiven king',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isUnlisted: false,
    isBorrowed: false,
  };

  const book3: CreateUpdateBookDTO = {
    name: 'The Hitchhiker\'s Guide to the Galaxy',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isUnlisted: false,
    isBorrowed: false,
  };

  const book4: CreateUpdateBookDTO = {
    name: 'The Interview with a vampire',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isUnlisted: false,
    isBorrowed: false,
  };

  const book5: CreateUpdateBookDTO = {
    name: 'The two towers',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book6: CreateUpdateBookDTO = {
    name: 'The fellowship of the ring',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book7: CreateUpdateBookDTO = {
    name: 'The Silmarillion',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book8: CreateUpdateBookDTO = {
    name: 'The hobbit',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book9: CreateUpdateBookDTO = {
    name: 'The lord of the rings',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book10: CreateUpdateBookDTO = {
    name: 'A song of ice and fire',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };
  const book11: CreateUpdateBookDTO = {
    name: 'The Shadow Over Innsmouth',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };
  const book12: CreateUpdateBookDTO = {
    name: 'A dance with dragons',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book13: CreateUpdateBookDTO = {
    name: 'A feast for crows',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book14: CreateUpdateBookDTO = {
    name: 'A storm of swords',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book15: CreateUpdateBookDTO = {
    name: 'A clash of kings',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book16: CreateUpdateBookDTO = {
    name: 'A game of thrones',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book17: CreateUpdateBookDTO = {
    name: 'Fight club',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book18: CreateUpdateBookDTO = {
    name: 'Ready player one',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book19: CreateUpdateBookDTO = {
    name: 'The Da Vinci code',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book20: CreateUpdateBookDTO = {
    name: 'Stalker',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book21: CreateUpdateBookDTO = {
    name: 'The call of Cthulhu',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book22: CreateUpdateBookDTO = {
    name: 'At the mountain of madness',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book23: CreateUpdateBookDTO = {
    name: 'World war Z',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book24: CreateUpdateBookDTO = {
    name: 'Patient zero',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book25: CreateUpdateBookDTO = {
    name: 'I am legend',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book26: CreateUpdateBookDTO = {
    name: 'The stand',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book27: CreateUpdateBookDTO = {
    name: 'You don\'t know js',
    author: 'Mr Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };
  const book28: CreateUpdateBookDTO = {
    name: 'Dracula',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book29: CreateUpdateBookDTO = {
    name: 'Metro',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book30: CreateUpdateBookDTO = {
    name: 'The witcher',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book31: CreateUpdateBookDTO = {
    name: 'Dune',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book32: CreateUpdateBookDTO = {
    name: 'I have no mouth and I must scream',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book33: CreateUpdateBookDTO = {
    name: 'H.P. Lovecraft\'s complete fiction',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book34: CreateUpdateBookDTO = {
    name: 'The Bible',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book35: CreateUpdateBookDTO = {
    name: 'Thinner',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book36: CreateUpdateBookDTO = {
    name: 'Misery',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book37: CreateUpdateBookDTO = {
    name: 'Lord of the flies',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book38: CreateUpdateBookDTO = {
    name: 'Don Quixote',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book39: CreateUpdateBookDTO = {
    name: 'Alice\'s adventures in wonderland',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book40: CreateUpdateBookDTO = {
    name: 'Tinker tailor soldier spy',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book41: CreateUpdateBookDTO = {
    name: 'Of mice and men',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book42: CreateUpdateBookDTO = {
    name: 'The call of the wild',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book43: CreateUpdateBookDTO = {
    name: 'For whom the bell tolls',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book44: CreateUpdateBookDTO = {
    name: 'The count of Monte Cristo',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book45: CreateUpdateBookDTO = {
    name: 'The Picture of Dorian Gray',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book46: CreateUpdateBookDTO = {
    name: 'A wrinkle in time',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book47: CreateUpdateBookDTO = {
    name: 'Hamlet',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book48: CreateUpdateBookDTO = {
    name: 'Iliad',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book49: CreateUpdateBookDTO = {
    name: 'The odyssey',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book50: CreateUpdateBookDTO = {
    name: 'Robinson crusoe',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book51: CreateUpdateBookDTO = {
    name: 'A clockwork orange',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book52: CreateUpdateBookDTO = {
    name: 'Peter Pan',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book53: CreateUpdateBookDTO = {
    name: 'Beowulf',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book54: CreateUpdateBookDTO = {
    name: 'The little prince',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book55: CreateUpdateBookDTO = {
    name: 'Frankenstein',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book56: CreateUpdateBookDTO = {
    name: 'Pro Git',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book57: CreateUpdateBookDTO = {
    name: 'The last of the mohicans',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book58: CreateUpdateBookDTO = {
    name: 'Strange Case of Dr Jekyll and Mr Hyde',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book59: CreateUpdateBookDTO = {
    name: 'Gone girl',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book60: CreateUpdateBookDTO = {
    name: 'Howling Dark ',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book61: CreateUpdateBookDTO = {
    name: 'Empire of silence',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book62: CreateUpdateBookDTO = {
    name: 'The dark tower',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book63: CreateUpdateBookDTO = {
    name: 'The wise man\'s fear',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book64: CreateUpdateBookDTO = {
    name: 'Good omens',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book65: CreateUpdateBookDTO = {

    name: 'The return of the king',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book66: CreateUpdateBookDTO = {
    name: 'The great Gatsby',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book67: CreateUpdateBookDTO = {
    name: 'The divine comedy',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book68: CreateUpdateBookDTO = {
    name: 'Memoirs of a Geisha',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  const book69: CreateUpdateBookDTO = {
    name: 'To be decided',
    author: 'Mr. Big Dum Lazy',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isBorrowed: false,
    isUnlisted: false,
  };

  await bookRepo.save(book1);
  console.log('Seeded book1 successfully!');

  await bookRepo.save(book2);
  console.log('Seeded book2 successfully!');

  await bookRepo.save(book3);
  console.log('Seeded book3 successfully!');

  await bookRepo.save(book4);
  console.log('Seeded book4 successfully!');

  await bookRepo.save(book5);
  console.log('Seeded book5 successfully!');

  await bookRepo.save(book6);
  console.log('Seeded book6 successfully!');

  await bookRepo.save(book7);
  console.log('Seeded book7 successfully!');

  await bookRepo.save(book8);
  console.log('Seeded book8 successfully!');

  await bookRepo.save(book9);
  console.log('Seeded book9 successfully!');

  await bookRepo.save(book10);
  console.log('Seeded book10 successfully!');

  await bookRepo.save(book11);
  console.log('Seeded book11 successfully!');

  await bookRepo.save(book12);
  console.log('Seeded book12 successfully!');

  await bookRepo.save(book13);
  console.log('Seeded book13 successfully!');

  await bookRepo.save(book14);
  console.log('Seeded book14 successfully!');

  await bookRepo.save(book15);
  console.log('Seeded book15 successfully!');

  await bookRepo.save(book16);
  console.log('Seeded book16 successfully!');

  await bookRepo.save(book17);
  console.log('Seeded book17 successfully!');

  await bookRepo.save(book18);
  console.log('Seeded book18 successfully!');

  await bookRepo.save(book19);
  console.log('Seeded book19 successfully!');

  await bookRepo.save(book20);
  console.log('Seeded book20 successfully!');

  await bookRepo.save(book21);
  console.log('Seeded book21 successfully!');

  await bookRepo.save(book22);
  console.log('Seeded book22 successfully!');

  await bookRepo.save(book23);
  console.log('Seeded book23 successfully!');

  await bookRepo.save(book24);
  console.log('Seeded book24 successfully!');

  await bookRepo.save(book25);
  console.log('Seeded book25 successfully!');

  await bookRepo.save(book26);
  console.log('Seeded book26 successfully!');

  await bookRepo.save(book27);
  console.log('Seeded book27 successfully!');

  await bookRepo.save(book28);
  console.log('Seeded book28 successfully!');

  await bookRepo.save(book29);
  console.log('Seeded book29 successfully!');

  await bookRepo.save(book30);
  console.log('Seeded book30 successfully!');

  await bookRepo.save(book31);
  console.log('Seeded book31 successfully!');

  await bookRepo.save(book32);
  console.log('Seeded book32 successfully!');

  await bookRepo.save(book33);
  console.log('Seeded book33 successfully!');

  await bookRepo.save(book34);
  console.log('Seeded book34 successfully!');

  await bookRepo.save(book35);
  console.log('Seeded book35 successfully!');

  await bookRepo.save(book36);
  console.log('Seeded book36 successfully!');

  await bookRepo.save(book37);
  console.log('Seeded book37 successfully!');

  await bookRepo.save(book38);
  console.log('Seeded book38 successfully!');

  await bookRepo.save(book39);
  console.log('Seeded book39 successfully!');

  await bookRepo.save(book40);
  console.log('Seeded book40 successfully!');

  await bookRepo.save(book41);
  console.log('Seeded book41 successfully!');

  await bookRepo.save(book42);
  console.log('Seeded book42 successfully!');

  await bookRepo.save(book43);
  console.log('Seeded book43 successfully!');

  await bookRepo.save(book44);
  console.log('Seeded book44 successfully!');

  await bookRepo.save(book45);
  console.log('Seeded book45 successfully!');

  await bookRepo.save(book46);
  console.log('Seeded book46 successfully!');

  await bookRepo.save(book47);
  console.log('Seeded book47 successfully!');

  await bookRepo.save(book48);
  console.log('Seeded book48 successfully!');

  await bookRepo.save(book49);
  console.log('Seeded book49 successfully!');

  await bookRepo.save(book50);
  console.log('Seeded book50 successfully!');

  await bookRepo.save(book51);
  console.log('Seeded book51 successfully!');

  await bookRepo.save(book52);
  console.log('Seeded book52 successfully!');

  await bookRepo.save(book53);
  console.log('Seeded book53 successfully!');

  await bookRepo.save(book54);
  console.log('Seeded book54 successfully!');

  await bookRepo.save(book55);
  console.log('Seeded book55 successfully!');

  await bookRepo.save(book56);
  console.log('Seeded book56 successfully!');

  await bookRepo.save(book57);
  console.log('Seeded book57 successfully!');

  await bookRepo.save(book58);
  console.log('Seeded book58 successfully!');

  await bookRepo.save(book59);
  console.log('Seeded book59 successfully!');

  await bookRepo.save(book60);
  console.log('Seeded book60 successfully!');

  await bookRepo.save(book61);
  console.log('Seeded book61 successfully!');

  await bookRepo.save(book62);
  console.log('Seeded book62 successfully!');

  await bookRepo.save(book63);
  console.log('Seeded book63 successfully!');

  await bookRepo.save(book64);
  console.log('Seeded book64 successfully!');

  await bookRepo.save(book65);
  console.log('Seeded book65 successfully!');

  await bookRepo.save(book66);
  console.log('Seeded book66 successfully!');

  await bookRepo.save(book67);
  console.log('Seeded book67 successfully!');

  await bookRepo.save(book68);
  console.log('Seeded book68 successfully!');

  await bookRepo.save(book69);
  console.log('Seeded book69 successfully! (~_o)');

};

//   ];
//   // console.log(booksForSaving);
//   // await asyncForEach(booksForSaving, async (book) => {
//   //   let bookForSaving = new Book();
//   //   bookForSaving = {... book};
//   //   // booksForSaving.ratingScale = Promise.resolve([])
//   //   console.log(bookForSaving)
//   //   await booksRepo.save(bookForSaving);

//   // });

//   booksForSaving.forEach(async (book) => {
//     const bookForSaving: Book = booksRepo.create(book);
//     // bookForSaving = {... book};
//     await booksRepo.save(bookForSaving);
//   });
//   return;
// };

const seedRoles = async connection => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Array<Promise<Role>> = Object.keys(UserRole).map(
    async (roleName: string) => {
      const role: Role = rolesRepo.create({ name: roleName });
      return await rolesRepo.save(role);
    },
  );
  await Promise.all(rolesSeeding);

  console.log('Seeded roles successfully!');
};

const seedAdmin = async connection => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const adminRole: Role = await rolesRepo.findOne({ name: UserRole.Admin });
  if (!adminRole) {
    console.log('The DB does not have an admin role!');
    return;
  }

  const newAdmin: User = userRepo.create({
    name: 'admin',
    email: 'admin@admin.com',
    password: 'Aaaa!0',
    roles: [adminRole],
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  // await seedRoles(connection);
  // await seedAdmin(connection);
  await seedBooks(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
