import {MigrationInterface, QueryRunner} from "typeorm";

export class library1568922315320 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `roles` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `flags` (`id` varchar(36) NOT NULL, `reason` varchar(255) NOT NULL, `flagedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `flagedReviewId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `votes` (`id` varchar(36) NOT NULL, `votedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `votedReviewId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `reviews` (`id` varchar(36) NOT NULL, `content` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `authorId` varchar(36) NULL, `bookId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `author` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `isBorrowed` tinyint NOT NULL DEFAULT 0, `isUnlisted` tinyint NOT NULL DEFAULT 0, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `ratingsId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `ratings` (`id` varchar(36) NOT NULL, `rating` int NOT NULL, `bookId` varchar(36) NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `joined` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `votesId` varchar(36) NULL, UNIQUE INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` (`name`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `booksStatus` (`id` varchar(36) NOT NULL, `borrowedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `returnedOn` datetime NULL, `userId` varchar(36) NULL, `bookId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_roles_roles` (`usersId` varchar(36) NOT NULL, `rolesId` varchar(36) NOT NULL, INDEX `IDX_df951a64f09865171d2d7a502b` (`usersId`), INDEX `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`), PRIMARY KEY (`usersId`, `rolesId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `flags` ADD CONSTRAINT `FK_808cb142f6cce4c341d8993af1b` FOREIGN KEY (`flagedReviewId`) REFERENCES `reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `votes` ADD CONSTRAINT `FK_13e27ad8e296f908453d522c258` FOREIGN KEY (`votedReviewId`) REFERENCES `reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews` ADD CONSTRAINT `FK_48770372f891b9998360e4434f3` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews` ADD CONSTRAINT `FK_cab4e55252a9c18a27e81415299` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books` ADD CONSTRAINT `FK_54c826651f482f46b9b7b354cba` FOREIGN KEY (`ratingsId`) REFERENCES `ratings`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ratings` ADD CONSTRAINT `FK_0563ca767066800a8b2123e6d15` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `ratings` ADD CONSTRAINT `FK_4d0b0e3a4c4af854d225154ba40` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_fa37facff1866d6e6492743e863` FOREIGN KEY (`votesId`) REFERENCES `votes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `booksStatus` ADD CONSTRAINT `FK_4c13ccb0a5e5d930fb2f2521202` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `booksStatus` ADD CONSTRAINT `FK_1d24b109e50ad7aad4c057a16df` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_b2f0366aa9349789527e0c36d97`");
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_df951a64f09865171d2d7a502b1`");
        await queryRunner.query("ALTER TABLE `booksStatus` DROP FOREIGN KEY `FK_1d24b109e50ad7aad4c057a16df`");
        await queryRunner.query("ALTER TABLE `booksStatus` DROP FOREIGN KEY `FK_4c13ccb0a5e5d930fb2f2521202`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_fa37facff1866d6e6492743e863`");
        await queryRunner.query("ALTER TABLE `ratings` DROP FOREIGN KEY `FK_4d0b0e3a4c4af854d225154ba40`");
        await queryRunner.query("ALTER TABLE `ratings` DROP FOREIGN KEY `FK_0563ca767066800a8b2123e6d15`");
        await queryRunner.query("ALTER TABLE `books` DROP FOREIGN KEY `FK_54c826651f482f46b9b7b354cba`");
        await queryRunner.query("ALTER TABLE `reviews` DROP FOREIGN KEY `FK_cab4e55252a9c18a27e81415299`");
        await queryRunner.query("ALTER TABLE `reviews` DROP FOREIGN KEY `FK_48770372f891b9998360e4434f3`");
        await queryRunner.query("ALTER TABLE `votes` DROP FOREIGN KEY `FK_13e27ad8e296f908453d522c258`");
        await queryRunner.query("ALTER TABLE `flags` DROP FOREIGN KEY `FK_808cb142f6cce4c341d8993af1b`");
        await queryRunner.query("DROP INDEX `IDX_b2f0366aa9349789527e0c36d9` ON `users_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_df951a64f09865171d2d7a502b` ON `users_roles_roles`");
        await queryRunner.query("DROP TABLE `users_roles_roles`");
        await queryRunner.query("DROP TABLE `booksStatus`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_51b8b26ac168fbe7d6f5653e6c` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `ratings`");
        await queryRunner.query("DROP TABLE `books`");
        await queryRunner.query("DROP TABLE `reviews`");
        await queryRunner.query("DROP TABLE `votes`");
        await queryRunner.query("DROP TABLE `flags`");
        await queryRunner.query("DROP TABLE `roles`");
    }

}
