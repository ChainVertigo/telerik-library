import { Controller, UseGuards, Get, Param, Put, Body, Req, Delete, Post, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BooksService } from './books.service';

import { ShowBookDTO } from '../models/book/book-show-dto';
import { ShowBooksDTO } from '../models/book/books-show-dto';
import { CreateUpdateBookDTO } from '../models/book/create-update-book-dto';
import { ShowBookStatusDTO } from '../models/book/bookStatus-show-dto';
import { BookBorrowsDTO } from '../models/book/book-borrows-dto';

@UseGuards(AuthGuard())
@Controller('api')
export class BooksController {

  constructor(
    private readonly bookService: BooksService,
  ) { }

  @Get('')
  async CreatorsChoice(
    @Query('page') page: number = 1,
    @Query('pageSize') pageSize: number = 10,
  ): Promise<ShowBooksDTO> {
    return this.bookService.creatorsChoice(page, pageSize);
  }

  @Get('books')
  async getAllBooks(
    @Query('page') page: number = 1,
    @Query('pageSize') pageSize: number = 10,
    @Query('searchText') searchText: string,
    @Query('searchAuthor') searchAuthor: string,
    @Query('searchName') searchName: string,
  ): Promise<ShowBooksDTO> {
    return this.bookService.findAll(page, pageSize, searchText, searchAuthor, searchName);
  }

  @Get('books/:id')
  async getSingleBook(@Param('id') id: string): Promise<BookBorrowsDTO> {
    return this.bookService.findBook(id);
  }

  @Put('books/update/:id')
  async updateBook(
    @Param('id') id: string,
    @Body() book: CreateUpdateBookDTO,
  ): Promise<ShowBookDTO> {
    return await this.bookService.update(id, book);
  }

  @Delete('books/unlist/:id')
  async unlistBook(
    @Param('id') id: string,
  ): Promise<ShowBookDTO> {
    return await this.bookService.unlist(id);
  }

  @Get('books/borrow/:id')
  async borrowBook(
    @Param('id') id: string,
    @Req() request,
  ): Promise<ShowBookStatusDTO> {
    return await this.bookService.borrowBook(id, request.user);
  }

  @Get('books/:id/return/:statusId')
  async returnBook(
    @Param('id') id: string,
    @Param('statusId') statusId: string,
    @Req() request,
  ): Promise<ShowBookStatusDTO> {
    return await this.bookService.retrunBook(id, request.user, statusId);
  }
}
