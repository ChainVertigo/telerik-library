
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';

import { Injectable, BadRequestException } from '@nestjs/common';

import { Book } from '../data/entities/book';
import { BookStatus } from '../data/entities/book-status';

import { ShowBookDTO } from '../models/book/book-show-dto';
import { ShowBooksDTO } from '../models/book/books-show-dto';
import { BookBorrowsDTO } from '../models/book/book-borrows-dto';
import { CreateUpdateBookDTO } from '../models/book/create-update-book-dto';
import { ShowBookStatusDTO } from '../models/book/bookStatus-show-dto';

@Injectable()
export class BooksService {
  
  constructor(
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(BookStatus) private readonly bookStatusRepository: Repository<BookStatus>,
    ) { }

    async creatorsChoice(page: number, pageSize: number): Promise<ShowBooksDTO> {
    const foundBooks = await this.bookRepository.find({
      where: {
        isUnlisted: false,
      },
      take: pageSize,
      skip: pageSize * (page - 1),
    });
    const count = await this.bookRepository.count({
      where: {
        isUnlisted: false,
      },
    });

    if (!foundBooks) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const convertedBooks: ShowBookDTO[] = [];

    foundBooks.map(async book => {
      const convertedBook: ShowBookDTO = { ... await this.convertToShowBookDTO(book) };
      convertedBooks.push(convertedBook);
    });

    const bookToReturn: ShowBooksDTO = { books: convertedBooks, count } as ShowBooksDTO;
    return bookToReturn;
    }

    async findAll(page: number, pageSize: number, searchText: string, searchAuthor: string, searchName: string): Promise<ShowBooksDTO> {
    let foundBooks;
    let count;

    if (!searchText || searchText === 'undefined' || searchText === '') {
      foundBooks = await this.bookRepository.find({
        where: {
          isUnlisted: false,
        },
        take: pageSize,
        skip: pageSize * (page - 1),
      });

      count = await this.bookRepository.count({
        where: {
          isUnlisted: false,
        },
      });

    } else if (searchName && searchName !== 'undefined' && searchName !== 'false' && searchAuthor && searchAuthor !== 'undefined' && searchAuthor !== 'false') {
      foundBooks = await this.bookRepository.find({
        where: {
          isUnlisted: false,
          name: Like(`%${searchText}%`),
          author: Like(`%${searchText}%`),
        },
        take: pageSize,
        skip: pageSize * (page - 1),
      });

      count = await this.bookRepository.count({
        where: {
          isUnlisted: false,
          name: Like(`%${searchText}%`),
          author: Like(`%${searchText}%`),
        },
      });

    } else if (searchName && searchName !== 'undefined' && searchName !== 'false') {
      foundBooks = await this.bookRepository.find({
        where: {
          isUnlisted: false,
          name: Like(`%${searchText}%`),
        },
        take: pageSize,
        skip: pageSize * (page - 1),
      });

      count = await this.bookRepository.count({
        where: {
          isUnlisted: false,
          name: Like(`%${searchText}%`),
        },
      });

    } else if (searchAuthor && searchAuthor !== 'undefined' && searchAuthor !== 'false') {
      foundBooks = await this.bookRepository.find({
        where: {
          isUnlisted: false,
          author: Like(`%${searchText}%`),
        },
        take: pageSize,
        skip: pageSize * (page - 1),
      });

      count = await this.bookRepository.count({
        where: {
          isUnlisted: false,
          author: Like(`%${searchText}%`),
        },
      });
    }

    if (!foundBooks) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const convertedBooks: ShowBookDTO[] = [];

    foundBooks.map(async book => {
      const convertedBook: ShowBookDTO = { ... await this.convertToShowBookDTO(book) };
      convertedBooks.push(convertedBook);
    });

    const bookToReturn: ShowBooksDTO = { books: convertedBooks, count } as ShowBooksDTO;
    return bookToReturn;

  }

  async findBook(id: string): Promise<BookBorrowsDTO> {
    const foundBook = await this.bookRepository.findOne({
      where: {
        id,
        isUnlisted: false,
      },
    });

    if (!foundBook) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const foundStatuses = await this.bookStatusRepository.find({
      relations: ['user'],
      where: {
        book: {
          id,
        },
      },
    });

    if (!foundStatuses) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    const convertedBook = await this.convertToShowBookDTO(foundBook);
    const convertedStatuses: ShowBookStatusDTO[] = [];

    foundStatuses.map(async status => {
      const convertedStatus: ShowBookStatusDTO = { ... await this.convertToShowBookStatusDTO(status) };
      convertedStatuses.push(convertedStatus);
    });

    const bookToReturn: BookBorrowsDTO = { book: convertedBook, statuses: convertedStatuses };
    return bookToReturn;
  }

  async update(id: string, book: CreateUpdateBookDTO): Promise<ShowBookDTO> {
    let bookToUpdate = await this.bookRepository.findOne({
      where: { id },
    });

    if (!bookToUpdate) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    bookToUpdate = { ...bookToUpdate, ...book };
    const updateBook = await this.bookRepository.save(bookToUpdate);

    return this.convertToShowBookDTO(updateBook);
  }

  async unlist(id: string): Promise<ShowBookDTO> {
    const bookToUnlist = await this.bookRepository.findOne({
      where: { id },
    });

    if (!bookToUnlist) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }

    bookToUnlist.isUnlisted = true;
    const unlistBooks = await this.bookRepository.save(bookToUnlist);

    return this.convertToShowBookDTO(unlistBooks);
  }

  async borrowBook(id: string, user): Promise<ShowBookStatusDTO> {

    const bookToBorrow = await this.bookRepository.findOne({
      where: { id },
    });

    if (!bookToBorrow) {
      throw new BadRequestException('Yare Yare Daze! Someting went wrong.');
    }
    if (bookToBorrow.isBorrowed === true) {
      throw new BadRequestException('Yare Yare Daze! Someone has laready borrowed that book.');
    }

    bookToBorrow.isBorrowed = true;
    const status = new BookStatus();
    status.user = user;
    status.book = bookToBorrow;
    const savedStatus = await this.bookStatusRepository.save(status);
    const borrowBooks = await this.bookRepository.save(bookToBorrow);
    return savedStatus;
  }

  async retrunBook(id: string, user, statusId: string): Promise<ShowBookStatusDTO> {
    const bookToReturn = await this.bookRepository.findOne({
      where: {
        id,
        isBorrowed: true,
      },
    });

    if (!bookToReturn) {
      throw new BadRequestException('Yare Yare Daze! Something went wrong.');
    }

    bookToReturn.isBorrowed = false;
    const status = await this.bookStatusRepository.findOne({
      where: {
        id: statusId,
      },
    });

    status.returnedOn = new Date();
    const savedStatus = await this.bookStatusRepository.save(status);
    const returnBooks = await this.bookRepository.save(bookToReturn);
    return savedStatus;
  }

  private async convertToShowBookDTO(book: Book): Promise<ShowBookDTO> {

    const convertedBook: ShowBookDTO = {
      id: book.id,
      name: book.name,
      description: book.description,
      author: book.author,
      isBorrowed: book.isBorrowed,
      isUnlisted: book.isUnlisted,
    };

    return convertedBook;
  }

  private async convertToShowBookStatusDTO(status: BookStatus): Promise<ShowBookStatusDTO> {

    const convertedStatus: ShowBookStatusDTO = {
      id: status.id,
      user: status.user,
      book: status.book,
      borrowedOn: status.borrowedOn,
      returnedOn: status.returnedOn,

    };
    return convertedStatus;
  }
}
