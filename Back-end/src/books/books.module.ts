import { Book } from '../data/entities/book';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';

import { BooksService } from './books.service';

import { BooksController } from './books.controller';

import { BookStatus } from '../data/entities/book-status';
import { User } from '../data/entities/user';

@Module({
  imports: [TypeOrmModule.forFeature([Book, BookStatus, User]), AuthModule],
  providers: [BooksService],
  controllers: [BooksController],
  exports: [BooksService],
})
export class BooksModule {}
