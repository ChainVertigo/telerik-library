import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from './core/services/auth.service';
import { NotificatorService } from './core/services/notificator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  public username = '';
  public isLogged = false;
  private subscription: Subscription;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly auth: AuthService,
    private readonly router: Router,
  ) {
  }

  ngOnInit() {
    this.subscription = this.auth.user$.subscribe(
      username => {
        if (username === null) {
          this.username = '';
          this.isLogged = false;
          // this.notificator.success(`You have logged out.`);
        } else {
          this.username = username;
          this.isLogged = true;
          // this.notificator.success(`Welcome, ${username}!`);
        }
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['home']);
    this.notificator.success(`You have logged out.`);
  }
}
