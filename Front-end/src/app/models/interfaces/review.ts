import { User } from './user';

export interface Review {
  id: string;
  author: User;
  content: string;
  isAuthor?: boolean;
  isDeleted: boolean;
  createdOn: Date;
  updatedOn: Date;
}
