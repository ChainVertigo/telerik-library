import { Book } from './book';
import { User } from './user';

export interface BookStatus {
  id: string;
  user: User;
  book: Book;
  borrowedOn: Date;
  returnedOn: Date;
}