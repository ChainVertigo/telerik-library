import { Review } from './review';

export interface Reviews {
  reviews: Review[];
  count: number;
}
