import { Book } from './book';

export interface Books {
  books: Book[];
  count: number;
}
