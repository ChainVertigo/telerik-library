export interface Book {
  id: string;
  name: string;
  author: string;
  description: string;
  isUnlisted: boolean;
  isBorrowed: boolean;
}
