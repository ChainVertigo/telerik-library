import { TestBed, async, inject, ComponentFixture } from '@angular/core/testing';
import { ReviewResolverService } from '../review/services/review-resolver.service';
import { ReviewComponent } from '../review/review.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CreateReviewComponent } from '../review/create-review/create-review.component';
import { UpdateReviewComponent } from '../review/update-review/update-review.component';
import { ReviewService } from '../review/services/review.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateUpdateReviewService } from '../review/services/create-update-review.service';
import { AuthService } from '../core/services/auth.service';
import { NotificatorService } from '../core/services/notificator.service';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs';


describe('ReviewComponent', () => {
  let component: ReviewComponent;
  let fixture: ComponentFixture<ReviewComponent>;

  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const reviewService = jasmine.createSpyObj('ReviewService', ['deleteReview']);
  const createUpdateReviewService = jasmine.createSpyObj('CreateUpdateReviewService', ['getNewReview', 'setNewReview', 'getUpdatedReview', 'setUpdatedReview']);
  const auth = jasmine.createSpyObj('AuthService', ['user$']);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);

  auth.user$ = of('Rostislav');

  activatedRoute.data = of({
    book: {
      book: [
        {
          author: 'Mr. Big Dum Lazy',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
          id: '133f6e78-cc08-4d61-8a06-442472ee40d9',
          isBorrowed: false,
          isUnlisted: false,
          name: 'The Picture of Dorian Gray'
        }
      ],
    },
    reviews: [
      {
        author: {
          id: '8eec35df-d8b9-4984-aebf-0914eb389d78',
          name: 'Rostislav',
          password: 'password',
          email: 'email@mail.com',
          joined: '2019-09-19T19:46:46.406Z',
          roles: []
        },
        id: 'update',
        content: 'aaaaaaaaaaaaaaaaa',
        createdOn: '2019-09-21T08:51:41.984Z',
        isAuthor: true,
        isDeleted: false,
        updatedOn: '2019-09-21T08:51:41.984Z'
      }
    ]
  });

  // reviewService.deleteReview.and.returnValue(of(

  // ));
  createUpdateReviewService.getNewReview.and.returnValue(of(
    {
      author: {
        id: '8eec35df-d8ba9-4984-aebf-0914eb389d78',
        name: 'Rostislav',
        email: 'email@mail.com',
        createdOn: '2019-09-19T19:46:46.406Z',
      },
      id: 'new',
      content: 'aaaaaaaaaaaaaaaaa',
      createdOn: '2019-09-21T08:51:41.984Z',
      isAuthor: true,
      isDeleted: false,
      updatedOn: '2019-09-21T08:51:41.984Z'
    }
  ));

  createUpdateReviewService.getUpdatedReview.and.returnValue(of(
    {
      author: {
        id: '8eec35df-d8b9-4984-aebf-0914eb389d78',
        name: 'Rostislav',
        email: 'email@mail.com',
        createdOn: '2019-09-19T19:46:46.406Z',
      },
      id: 'update',
      content: 'updated',
      createdOn: '2019-09-21T08:51:41.984Z',
      isAuthor: true,
      isDeleted: false,
      updatedOn: '2019-09-21T08:51:41.984Z'
    }
  ));
  // reviewService.deleteReview.and.returnValue(of(
  //   {
  //     reviews: [
  //       {
  //         id: 'test-id',
  //         content: 'content',
  //         createdOn: '2019-09-21T08:51:41.984Z',
  //         updatedOn: '2019-09-21T08:51:41.984Z',
  //         isAuthor: true,
  //         isDeleted: false,
  //       },
  //       {
  //         id: 'test-id2',
  //         content: 'content2',
  //         createdOn: '2019-09-21T08:51:41.984Z',
  //         updatedOn: '2019-09-21T08:51:41.984Z',
  //         isAuthor: true,
  //         isDeleted: false,
  //       },
  //       {
  //         id: 'test-id3',
  //         content: 'content3',
  //         createdOn: '2019-09-21T08:51:41.984Z',
  //         updatedOn: '2019-09-21T08:51:41.984Z',
  //         isAuthor: true,
  //         isDeleted: false,
  //       },
  //       {
  //         id: 'test-id4',
  //         content: 'content4',
  //         createdOn: '2019-09-21T08:51:41.984Z',
  //         updatedOn: '2019-09-21T08:51:41.984Z',
  //         isAuthor: true,
  //         isDeleted: false,
  //       },
  //       {
  //         id: 'test-id5',
  //         content: 'content5',
  //         createdOn: '2019-09-21T08:51:41.984Z',
  //         updatedOn: '2019-09-21T08:51:41.984Z',
  //         isAuthor: true,
  //         isDeleted: false,
  //       },
  //     ],
  //   }
  // ));
  // tslint:disable-next-line: no-unused-expression
  createUpdateReviewService.setUpdatedReview;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        ReviewComponent,
        CreateReviewComponent,
        UpdateReviewComponent,
      ],
      imports: [
        CommonModule,
        SharedModule,
      ],
      providers: [
        {
          provide: ReviewService,
          useValue: reviewService,
        },
        {
          provide: Location,
          useValue: location,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: CreateUpdateReviewService,
          useValue: createUpdateReviewService,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        }

      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('ReviewComponent should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with the correct data', () => {
    component.bookId = '133f6e78-cc08-4d61-8a06-442472ee40d9';
    expect(component.bookId).toBe('133f6e78-cc08-4d61-8a06-442472ee40d9');
  });

  it('should set username correctly', () => {
    expect(component.username).toBe('Rostislav');
  });

  it('should initialize with the correct reviews', () => {
    expect(component.reviews[0].id).toBe('update');
    expect(component.reviews[0].content).toBe('updated');
    // expect(component.reviews[0].createdOn).toBe('2019-09-21T08:51:41.984Z');
    expect(component.reviews[0].isAuthor).toBe(true);
    expect(component.reviews[0].isDeleted).toBe(false);
    // expect(component.reviews[0].updatedOn).toBe('2019-09-21T08:51:41.984Z');
  });

  it('should initialize getNewReiew with the correct review data', () => {
    expect(component.reviews.length).toEqual(6);
  });

  // it('should initialize getUodatedReview with the correct review data', () => {
  //  component.updateReview({id:'update',content:'updated'});
  //   component.reviews.map(review => {
  //     if (review.id === 'update') {
  //       expect(review.content).toBe('updated');
  //     }
  //   });
  // });

//   it('should deleteReview', () => {
//     component.deleteReview('update');
//     let foundReview = false;
//     component.reviews.map(review => {
//       if (review.id === 'update') {
//         foundReview = true;
//       }
//     });
//     expect(foundReview).toBe(false);
// });
});
