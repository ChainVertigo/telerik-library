/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ReviewService } from '../../review/services/review.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('Service: Review', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: HttpClient,
        useValue: http,
      }]
    });
  });

  it('should ...', inject([ReviewService], (service: ReviewService) => {
    expect(service).toBeTruthy();
  }));

  it('getReviewsByBook should return the correct data', () => {
    const service: ReviewService = TestBed.get(ReviewService);
    http.get.and.returnValue(of(
      {
        reviews: [{
          id: 'review-id',
          content: 'review-content',
        }]
      }));
      service.getReviewsByBook(1, 5, '').subscribe(
        (data) => {
          expect(data.reviews[0].id).toBe('review-id');
          expect(data.reviews[0].content).toBe('review-content');
        }
      );
  });

  it('getReviewsByBook should call http.get one time', () => {
    const service: ReviewService = TestBed.get(ReviewService);
    http.get.calls.reset();
    service.getReviewsByBook(1, 5, '').subscribe(
      (data) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('createReview should create a review', () => {
    const service: ReviewService = TestBed.get(ReviewService);
    http.post.and.returnValue(of(
      {
        review: {
          id: 'test-id',
          content: 'content',
        }
      }
    ));
    service.createReview('content', 'bookId').subscribe(
      (data) => {
        expect(data.review.id).toBe('test-id');
        expect(data.review.content).toBe('content');
      }
    );
  });
});
