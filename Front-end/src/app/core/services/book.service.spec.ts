/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BookService } from '../../book/services/book.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('BookService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: HttpClient,
        useValue: http,
      }]
    });
  });

  it('should ...', inject([BookService], (service: BookService) => {
    expect(service).toBeTruthy();
  }));

  it('getBooks should return the correct data', () => {
    const service: BookService = TestBed.get(BookService);
    http.get.and.returnValue(of(
      {
        books: [{
          id: 'test-id',
          name: 'Test Name',
          description: 'test description',
        }],
        count: 10,
      }
    ));

    service.getBooks(1, 10, '').subscribe(
      (data) => {
        expect(data.books[0].id).toBe('test-id');
        expect(data.count).toBe(10);
      }
    );
  });

  it('getBooks should call http.get one time', () => {
    const service: BookService = TestBed.get(BookService);
    http.get.calls.reset();
    service.getBooks(1, 10, '').subscribe(
      (data) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('individualBook should return a single book', () => {
    const service: BookService = TestBed.get(BookService);
    http.get.and.returnValue(of(
      { book: {
        id: 'test-id',
        name: 'Test Name',
        author: 'Test Author',
        description: 'test description'
      }}
    ));
    service.idividualBook('test-id').subscribe(
      (data) => expect(data.book.name).toBe('Test Name')
    );
  });

  it('borrowBook should set isborrowed to true', () => {
    const service: BookService = TestBed.get(BookService);
    http.get.and.returnValue(of(
      {book: {
        id: 'test-id',
        isBorrowed: true
      }}
    ));
    service.borrowBook('test-id').subscribe(
      (data) => expect(data.book.isBorrowed).toBe(true)
    );
  });

  it('returnBook should set isBorrowed to false', () => {
    const service: BookService = TestBed.get(BookService);
    http.get.and.returnValue(of(
      {book: {
        id: 'test-id',
        statusId: 'status-id',
        isBorrowed: false
      }}
    ));
    service.returnBook('test-id', 'status-id').subscribe(
      (data) => expect(data.book.isBorrowed).toBe(false)
    );
  });
});
