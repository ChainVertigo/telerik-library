import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { BookComponent } from './book/book.component';
import { BookViewComponent } from './book/book-view/book-view.component';


import { BookResolverService } from './book/services/book-resolver.service';
import { ReviewResolverService } from './review/services/review-resolver.service';

import { SingleBookResolverService } from './book/services/single-book-resolver.service';
import { CreatorsChoiceResolveService } from './book/services/creators-choice-resolve.service';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, resolve: {books: CreatorsChoiceResolveService}},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'books', component: BookComponent, resolve: {books: BookResolverService}},
  {path: 'books/:id', component: BookViewComponent, resolve : {book: SingleBookResolverService, reviews: ReviewResolverService}},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
