import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { BookModule } from './book/book.module';
import { SharedModule } from './shared/shared.module';
import { ReviewModule } from './review/review.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

import { TokenInterceptorService } from './auth/token-interceptor.service';

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      LoginComponent,
      NavbarComponent,
      RegisterComponent,
      SidebarComponent,
      NotFoundComponent,
   ],
   imports: [
      AppRoutingModule,
      CoreModule,
      BookModule,
      SharedModule,
      ReviewModule
   ],
   providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptorService,
        multi: true
      },
    ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
