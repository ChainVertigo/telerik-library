import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ReviewService } from '../services/review.service';
import { CreateUpdateReviewService } from '../services/create-update-review.service';
import { NotificatorService } from '../../core/services/notificator.service';

import { Review } from '../../models/interfaces/review';

@Component({
  selector: 'app-update-review',
  templateUrl: './update-review.component.html',
  styleUrls: ['./update-review.component.css']
})
export class UpdateReviewComponent implements OnInit {

  constructor(
    private readonly reviewService: ReviewService,
    private readonly updateReviewService: CreateUpdateReviewService,
    private readonly notificator: NotificatorService,
    private readonly fb: FormBuilder,
  ) { }

  @Input() bookId: string;
  @Output() updateReview: EventEmitter<any> = new EventEmitter<any>(null);
  @Output() closeEditMode: EventEmitter<boolean> = new EventEmitter<boolean>(null);
  myForm: FormGroup;
  review: Review;

  ngOnInit() {
    this.myForm = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(10)]]
    });
    this.updateReviewService.getReviewForUpdate().subscribe(
      (data) => {
        this.review = data;
        this.myForm.controls['content'].setValue(this.review.content);

      }
    );
  }
  updatereview(form) {
    this.reviewService.updateReview(form.value.content, this.review.id, this.bookId).subscribe(
      (res: any) => {
        res.isAuthor = true;
        this.updateReviewService.setUpdatedReview(res);
        this.closeEditMode.emit(true);
        this.notificator.success('You have successfully updated a review!');
        form.reset();
      },
      () => {
        this.notificator.error('The review failed to updaaaaate!');
      }
    );
  }

  close() {
    this.closeEditMode.emit(true);
  }
}
