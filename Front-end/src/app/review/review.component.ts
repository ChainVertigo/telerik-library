import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

import { ReviewService } from './services/review.service';
import { CreateUpdateReviewService } from './services/create-update-review.service';
import { AuthService } from '../core/services/auth.service';
import { NotificatorService } from '../core/services/notificator.service';

import { Review } from '../models/interfaces/review';
import { Reviews } from '../models/interfaces/reviews';



@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  @Output() enableEditMode: EventEmitter<boolean> = new EventEmitter;
  @Input() bookId: string;
  reviews: Review[];
  showUpdateButton = true;
  showDeleteButton = true;
  page = 1;
  pageSize = 10;
  collectionSize: number;
  username = '';
  private subscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly reviewService: ReviewService,
    private readonly createUpdateReviewService: CreateUpdateReviewService,
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.subscription = this.auth.user$.subscribe(
      username => {
        if (username === null) {
          this.username = '';
        } else {
          this.username = username;
        }
      }
    );

    this.activatedRoute.data.subscribe(data => {
      this.reviews = data.reviews;
      this.reviews.map(review => {
        if (review.author.name === this.username) {
          review.isAuthor = true;
        }
      });
    });
    this.createUpdateReviewService.getNewReview().subscribe(review => {
      if (review) {
        this.reviews.push(review);
        this.createUpdateReviewService.setNewReview(null);
      }
    });

    this.createUpdateReviewService.getUpdatedReview().subscribe(updatedReview => {
      if (updatedReview) {
        const index = this.reviews.findIndex(review => review.id === updatedReview.id);
        this.reviews[index] = updatedReview;
        this.createUpdateReviewService.setUpdatedReview(null);
      }
    });
  }

  deleteReview(reviewId): void {
    this.reviewService.deleteReview(reviewId, this.bookId).subscribe(
      res => {
        const reviewIndex = this.reviews.findIndex(review => review.id === reviewId);
        this.reviews.splice(reviewIndex, 1);
        this.notificator.success('You have successfully deleted the review!');
      },
      () => {
        this.notificator.error('The review failed to Deleteeee!');
      }
    );
  }

  updateReview(review) {
    this.createUpdateReviewService.setReviewForUpdate(review);
    this.enableEditMode.emit(true);

  }


}
