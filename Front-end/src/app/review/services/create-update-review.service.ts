import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Review } from 'src/app/models/interfaces/review';

@Injectable({
  providedIn: 'root'
})
export class CreateUpdateReviewService {

  public newReview = new BehaviorSubject<Review>(null);
  public updatedReview = new BehaviorSubject<Review>(null);
  public reviewForUpdate = new BehaviorSubject<Review>(null);

  constructor() { }

  getNewReview() {
    return this.newReview.asObservable();
  }
  setNewReview(newReview) {
    this.newReview.next(newReview);
  }

  getUpdatedReview() {
    return this.updatedReview.asObservable();
  }
  setUpdatedReview(updatedReview) {
    this.updatedReview.next(updatedReview);
  }

  getReviewForUpdate() {
    return this.reviewForUpdate.asObservable();
  }
  setReviewForUpdate(reviewForUpdate) {
    this.reviewForUpdate.next(reviewForUpdate);
  }
}
