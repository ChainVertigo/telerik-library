import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ReviewService } from './review.service';
import { NotificatorService } from '../../core/services/notificator.service';

import { Review } from '../../models/interfaces/review';

@Injectable({
  providedIn: 'root'
})
export class ReviewResolverService implements Resolve<Review[] | {reviews: Review[]}> {

  constructor(
    private readonly reviewService: ReviewService,
    private readonly notificator: NotificatorService,
    private readonly activatedRoute: ActivatedRoute,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const bookId = route.paramMap.get('id');
    return this.reviewService.getAllReviews(bookId)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({reviews: null});
        }
      ));
  }
}
