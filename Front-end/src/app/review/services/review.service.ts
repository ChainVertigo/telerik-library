import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Review } from 'src/app/models/interfaces/review';
import { Reviews } from 'src/app/models/interfaces/reviews';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  getReviewsByBook(page = 1, pageSize = 5, id): Observable<Reviews> {
    return this.http.get<Reviews>(
      `http://localhost:3000/api/books/${id}/reviews/?page=${page}&pageSize=${pageSize}`
    );
  }

    getAllReviews(bookId: string): Observable<Review[]> {
      return this.http.get<Review[]>(`http://localhost:3000/api/books/${bookId}/reviews`);
    }

    createReview(content: string, bookId): Observable<{review: Review}> {
      return this.http.post<{review: Review}>(`http://localhost:3000/api/books/${bookId}/reviews`, {
        review: {content}
      });
    }

    deleteReview(id: string, bookid: string): Observable<{review: Review}> {
      return this.http.delete<{review: Review}>(`http://localhost:3000/api/books/${bookid}/reviews/${id}`);
    }

    updateReview(content: string, id: string, bookId: string): Observable<{review: Review}> {
      return this.http.put<{review: Review}>(`http://localhost:3000/api/books/${bookId}/reviews/${id}`, {
        review: {content}
      });
    }
}
