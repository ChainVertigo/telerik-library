import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ReviewService } from '../services/review.service';
import { CreateUpdateReviewService } from '../services/create-update-review.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.component.html',
  styleUrls: ['./create-review.component.css']
})
export class CreateReviewComponent implements OnInit {

  constructor(
  private readonly reviewService: ReviewService,
  private readonly notificator: NotificatorService,
  private readonly createReviewService: CreateUpdateReviewService) {}

 @Input() public bookId = null;
 @Output() public createdReview: EventEmitter<any> = new EventEmitter<any>(null);

  public createNewReview(form) {
    this.reviewService.createReview(form.value.content, this.bookId).subscribe(
      (res: any) => {
        res.isAuthor = true;
        this.createReviewService.setNewReview(res);
        this.notificator.success('You have successfully created a review!');
        form.reset();
      },
      () => {
        this.notificator.error('The review failed to createeeeeeeeeeeee!');
      }
    );
  }

  ngOnInit() { }

}
