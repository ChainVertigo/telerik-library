import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ReviewComponent } from './review.component';
import { CreateReviewComponent } from './create-review/create-review.component';

import { ReviewService } from './services/review.service';
import { ReviewResolverService } from './services/review-resolver.service';
import { UpdateReviewComponent } from './update-review/update-review.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ],
  declarations: [ReviewComponent, CreateReviewComponent, UpdateReviewComponent],
  providers: [ReviewService, ReviewResolverService],
  exports: [ReviewComponent, CreateReviewComponent, UpdateReviewComponent]
})
export class ReviewModule { }
