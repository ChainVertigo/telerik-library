import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input()
  public loggedIn;

  @Input()
  public username;

  @Output()
  public toggle = new EventEmitter<undefined>();

  @Output()
  public logout = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() {
    this.toggle.emit();
  }

  triggerLogout() {
    this.logout.emit();
  }

}
