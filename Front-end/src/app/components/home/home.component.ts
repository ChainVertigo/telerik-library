import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Book } from 'src/app/models/interfaces/book';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 books: Book[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.books = data.books.books;
    });
  }

}
