import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  constructor(
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  register(name: string, email: string, password: string) {
    this.auth.register(name, email, password).subscribe(
      result => {
        this.auth.login(name, password).subscribe(
          response => {
            this.notificator.success(`Welcome, ${response.user.name}!`);
            this.router.navigate(['home']);
          },
          error => this.notificator.error(error.message),
        );
      },
      error => this.notificator.error(error.message),
    );
  }

}
