import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
    private readonly routed: Router,
  ) { }

  ngOnInit() {
  }

  triggerLogin(name: string, password: string) {
    this.auth.login(name, password).subscribe(
      (result: any) => {
        this.notificator.success(`Welcome, ${result.user.name}!`);
        this.routed.navigate(['home']);
      },
      error => this.notificator.error(error.message),
    );
  }

}
