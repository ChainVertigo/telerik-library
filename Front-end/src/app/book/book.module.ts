import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoutingModule } from './book-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ReviewModule } from '../review/review.module';

import { BookComponent } from './book.component';
import { BookViewComponent } from './book-view/book-view.component';

import { BookService } from './services/book.service';

import { BookResolverService } from './services/book-resolver.service';
import { SingleBookResolverService } from './services/single-book-resolver.service';
import { CreatorsChoiceResolveService } from './services/creators-choice-resolve.service';


@NgModule({
  declarations: [
    BookComponent,
    BookViewComponent,
],
  imports: [
    SharedModule,
    CommonModule,
    BookRoutingModule,
    ReviewModule,
  ],
  providers: [BookService, BookResolverService, SingleBookResolverService, CreatorsChoiceResolveService],
})
export class BookModule { }
