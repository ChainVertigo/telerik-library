import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { BookComponent } from './book.component';
import { BookViewComponent } from './book-view/book-view.component';

import { BookResolverService } from './services/book-resolver.service';
import { SingleBookResolverService } from './services/single-book-resolver.service';
import { ReviewResolverService } from '../review/services/review-resolver.service';

const bookRoutes: Routes = [
  {
    path: '',
    component: BookComponent,
    resolve: {
      recipes: BookResolverService,
    },
  },
  {
    path: ':id',
    component: BookViewComponent,
    resolve: {book: SingleBookResolverService, reviews: ReviewResolverService},
  },
];

@NgModule({
  imports: [RouterModule.forChild(bookRoutes)],
  exports: [RouterModule],
})
export class BookRoutingModule {}
