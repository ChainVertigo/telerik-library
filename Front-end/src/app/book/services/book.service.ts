import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from 'src/app/models/interfaces/book';
import { Books } from 'src/app/models/interfaces/books';
import { BookStatus } from 'src/app/models/interfaces/status';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  getBooks(page = 1, pageSize = 10, searchQuery): Observable<Books> {
    return this.http.get<Books>(
     `http://localhost:3000/api/books?page=${page}&pageSize=${pageSize}&searchText=${searchQuery.searchText}&searchAuthor=${searchQuery.searchAuthor}&searchName=${searchQuery.searchName}`
    );
  }

  creatorsChoice(page = 1, pageSize = 10): Observable<Books> {
    return this.http.get<Books>(`http://localhost:3000?page=${page}&pageSize=${pageSize}`);
  }

  idividualBook(id: string): Observable<{ book: Book, statuses: BookStatus[] }> {
    return this.http.get<{ book: Book, statuses: BookStatus[] }>(`http://localhost:3000/api/books/${id}`);
  }

  borrowBook(id: string): Observable<BookStatus> {
    return this.http.get<BookStatus>(`http://localhost:3000/api/books/borrow/${id}`);
  }

  returnBook(id: string, statusId): Observable<BookStatus> {
    return this.http.get<BookStatus>(`http://localhost:3000/api/books/${id}/return/${statusId}`);
  }
}


