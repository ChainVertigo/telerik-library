import { Injectable } from '@angular/core';

import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { BookService } from 'src/app/book/services/book.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

import { Books } from 'src/app/models/interfaces/books';

@Injectable({
  providedIn: 'root'
})
export class CreatorsChoiceResolveService implements Resolve<Books| {books: Books}> {

  constructor(
    private readonly bookService: BookService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.bookService.creatorsChoice(5, 10 )
    .pipe(catchError(
      res => {
        this.notificator.error(res.error.error);
        return of({books: null});
      }
    ));
  }
}
