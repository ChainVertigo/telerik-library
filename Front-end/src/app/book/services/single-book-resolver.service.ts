import { Injectable } from '@angular/core';

import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { BookService } from 'src/app/book/services/book.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

import { Book } from 'src/app/models/interfaces/book';

@Injectable({
  providedIn: 'root'
})
export class SingleBookResolverService implements Resolve<Book | {book: Book}> {

  constructor(
    private readonly bookService: BookService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const bookId = route.paramMap.get('id');
    return this.bookService.idividualBook(bookId)
      .pipe(catchError(
        res => {
          this.router.navigate(['**']);
          return of({book: null, statuses: null});
        }
      ));
  }
}
