import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Book } from '../../models/interfaces/book';
import { BookStatus } from '../../models/interfaces/status';

import { BookService } from '../services/book.service';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.css']
})
export class BookViewComponent implements OnInit {

  editMode = false;
  canUserWriteReview = false;
  book: Book;
  statuses: BookStatus[];
  statusId = '';
  username = '';
  private subscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly bookService: BookService,
    private readonly auth: AuthService,
  ) { }

  ngOnInit() {
    this.subscription = this.auth.user$.subscribe(
      username => {
        if (username === null) {
          this.username = '';
        } else {
          this.username = username;
        }
      }
    );

    this.activatedRoute.data.subscribe(data => {
      this.book = data.book.book;
      this.statuses = data.book.statuses;
      this.statuses.map(status => {
        const hasUserBorrowed: boolean = status.user.name === this.username;
        if (hasUserBorrowed) {
          if (status.returnedOn) {
            this.canUserWriteReview = true;
          } else {
            this.statusId = status.id;
          }
        }
      });
    });
  }

  borrow() {
    this.bookService.borrowBook(this.book.id).subscribe(data => { 
      this.book.isBorrowed = true; 
      this.statusId = data.id; },
      err => console.log('error'));

  }

  return() {
    this.bookService.returnBook(this.book.id, this.statusId).subscribe(data => {
      this.book.isBorrowed = false; this.canUserWriteReview = true;
    },
      err => console.log('error'));
  }

}
