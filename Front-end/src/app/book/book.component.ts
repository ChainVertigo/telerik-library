import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { BookService } from './services/book.service';

import { Book } from '../models/interfaces/book';
import { Books } from '../models/interfaces/books';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  books: Book[];
  length: number;
  page = 1;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  searchForm: FormGroup;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly bookService: BookService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.books = data.books.books;
      this.length = data.books.count;
    });

    this.searchForm = this.fb.group({
      searchText: '',
      searchAuthor: '',
      searchName: '',
    });
    this.searchForm.controls['searchName'].setValue(true);
  }

  public getBooks(_page?: number): void {
    let searchPage;
    if (_page) {
      searchPage = _page;
    } else {
      searchPage = this.page;
    }

    this.bookService.getBooks(searchPage, this.pageSize, this.searchForm.value).subscribe(
      (books: Books) => {
        this.books = books.books;
        this.length = books.count;
      }
    );
  }


  public onPaginationChange(pageEvent): void {
    this.page = pageEvent.pageIndex + 1;
    this.pageSize = pageEvent.pageSize;
    console.log(pageEvent);
    this.getBooks();
  }

  public searchBooks(event) {
    this.getBooks();
  }
}
